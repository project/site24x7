<?php

namespace Drupal\site24x7\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Site24x7 Settings Form.
 *
 * @internal
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'site24x7.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'site24x7_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('site24x7.settings');

    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General'),
      '#description' => $this->t('Please paste your Site24x7 RUM Key here.'),
    ];
    $form['general']['rum_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('RUM Key'),
      '#default_value' => $config->get('rum_key'),
      '#description' => $this->t('For steps to obtain a RUM key, please see the README.md file.'),
      '#required' => TRUE,
    ];
    $form['general']['datacentre'] = [
      '#type' => 'select',
      '#title' => $this->t('Datacentre'),
      '#default_value' => $config->get('datacentre') ?? 'com',
      '#options' => [
        'com' => $this->t('United States'),
        'eu' => $this->t('Europe'),
        'in' => $this->t('India'),
        'au' => $this->t('Australia'),
        'cn' => $this->t('China'),
      ],
      '#description' => $this->t('Select your Site24x7 datacentre.'),
      '#required' => TRUE,
    ];

    // Visibility settings.
    $form['monitoring_scope'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Monitoring scope'),
    ];

    // Page specific visibility configurations.
    $visibility_request_path_pages = $config->get('visibility.request_path_pages');

    $form['monitoring']['page_visibility_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Pages'),
      '#group' => 'monitoring_scope',
    ];

    if ($config->get('visibility.request_path_mode') == 2) {
      $form['monitoring']['page_visibility_settings']['visibility_request_path_mode'] = [
        '#type' => 'value',
        '#value' => 2,
      ];
      $form['monitoring']['page_visibility_settings']['visibility_request_path_pages'] = [
        '#type' => 'value',
        '#value' => $visibility_request_path_pages,
      ];
    }
    else {
      $options = [
        $this->t('Every page except the listed pages'),
        $this->t('The listed pages only'),
      ];
      $description = $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", [
        '%blog' => '/blog',
        '%blog-wildcard' => '/blog/*',
        '%front' => '<front>',
      ]);

      $form['monitoring']['page_visibility_settings']['visibility_request_path_mode'] = [
        '#type' => 'radios',
        '#title' => $this->t('Add monitoring to specific pages'),
        '#options' => $options,
        '#default_value' => $config->get('visibility.request_path_mode'),
      ];
      $form['monitoring']['page_visibility_settings']['visibility_request_path_pages'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Pages'),
        '#title_display' => 'invisible',
        '#default_value' => !empty($visibility_request_path_pages) ? $visibility_request_path_pages : '',
        '#description' => $description,
        '#rows' => 10,
      ];
    }

    // Render the role overview.
    $visibility_user_role_roles = $config->get('visibility.user_role_roles');

    $form['monitoring']['role_visibility_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles'),
      '#group' => 'monitoring_scope',
    ];

    $form['monitoring']['role_visibility_settings']['visibility_user_role_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add RUM script for specific roles'),
      '#options' => [
        $this->t('Add to the selected roles only'),
        $this->t('Add to every role except the selected ones'),
      ],
      '#default_value' => $config->get('visibility.user_role_mode'),
    ];
    $form['monitoring']['role_visibility_settings']['visibility_user_role_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#default_value' => !empty($visibility_user_role_roles) ? $visibility_user_role_roles : [],
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', user_role_names()),
      '#description' => $this->t('If none of the roles are selected, all users will trigger monitoring. If a user has any of the roles checked, that user will be monitored (or excluded, depending on the setting above).'),
    ];

    // Only show this when there's no key entered else obviously the user has
    // an account.
    if (empty($config->get('rum_key'))) {
      $form['guff'] = [
        '#markup' => $this->t('If you don\'t have a Site24x7 account yet. Please <span><a target="_blank" href=":link">register here</a> for a free account.', [
          ':link' => Url::fromUri('https://www.site24x7.com/signup.html', [
            'query' => [
              'pack' => 4,
              'l' => 'en',
            ],
          ])->toString()
        ]),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Validate the RUM key.
    if (!preg_match("/[\'a-z0-9\']{24,34}/i", $form_state->getValue('rum_key'))) {
      $form_state->setErrorByName('rum_key', $this->t('Invalid RUM key.'));
    }

    $form_state->setValue('visibility_request_path_pages', trim($form_state->getValue('visibility_request_path_pages')));
    $form_state->setValue('visibility_user_role_roles', array_filter($form_state->getValue('visibility_user_role_roles') ?? []));

    // Verify that every path is prefixed with a slash, but don't check PHP
    // code snippets and do not check for slashes if no paths configured.
    if ($form_state->getValue('visibility_request_path_mode') != 2 && !empty($form_state->getValue('visibility_request_path_pages'))) {
      $pages = preg_split('/(\r\n?|\n)/', $form_state->getValue('visibility_request_path_pages'));
      foreach ($pages as $page) {
        if (!str_starts_with($page, '/') && $page !== '<front>') {
          $form_state->setErrorByName('visibility_request_path_pages', $this->t('Path "@page" not prefixed with slash.', ['@page' => $page]));
          // Drupal forms show one error only.
          break;
        }
      }
    }

  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('site24x7.settings')
      ->set('rum_key', $form_state->getValue('rum_key'))
      ->set('datacentre', $form_state->getValue('datacentre'))
      ->set('visibility.request_path_mode', $form_state->getValue('visibility_request_path_mode'))
      ->set('visibility.request_path_pages', $form_state->getValue('visibility_request_path_pages'))
      ->set('visibility.user_role_mode', $form_state->getValue('visibility_user_role_mode'))
      ->set('visibility.user_role_roles', $form_state->getValue('visibility_user_role_roles'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
